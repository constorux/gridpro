GridPro
==

GridPro is a simple sudoku solver written in python. It currently is able to solve easy to medium puzzles.
I am currently working on making it able to solve even harder puzzles.

I developed this mostly to obtain the basics about the Python programming language, ttherefore development of this script might not continue